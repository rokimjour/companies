var Company = function (ID, name, earnings, parentID) {
	this._id = ID
	this.name = name
	this.earnings = earnings
	this.parentID = parentID
}

var database = null

function initDatabase() {
	database = new PouchDB('companies');

	database.changes({
		since: 'now',
		live: true
	}).on('change', function () {
		displayCompanies()
		reloadUI()
	});
}

function getCompanies(callback) {
	database.allDocs({
		include_docs: true,
		descending: true
	}, function (err, doc) {
		var companies = doc.rows.map(function (obj) {
			return obj.doc
		})
		callback(companies)
	});
}

function getCompany(id, callback) {
	database.get(id).then(callback)
}

function displayCompanies() {
	getCompanies(function (companies) {
		console.log(companies)
	});
}

function addCompany(company) {
	database.put(company)
}

function deleteCompany(companyID) {
    getCompanies(function(companies) {
        getCompany(companyID, function(company) {
            deleteCompanyWithChildren(companies, company)
        });
    })
}

function editCompany(company) {
	getCompany(company._id, function (object) {
		company._rev = object._rev
		database.put(company)
	});

}

function destroyDB() {
	database.destroy().then(function (response) {
		// success
	}).catch(function (err) {
		console.log(err);
	});
}

function deleteCompanyWithChildren(companies, rootCompany) {
    var children = companies.filter(function(company) {
        return company.parentID == rootCompany._id
    })

    children.forEach(function(childCompany) {
        deleteCompanyWithChildren(companies, childCompany)
    })
    database.remove(rootCompany)
}

function sumEarningsOfCompany(companies, rootCompany) {
    var children = companies.filter(function(company) {
        return company.parentID == rootCompany._id
    })
    
    if (children.length == 0) {
        return 0
    }
    
    var sum = parseInt(rootCompany.earnings, 10)
    children.forEach(function(childCompany) {
        sum += sumEarningsOfCompany(companies, childCompany)
    })
    return sum
}