function reloadUI() {
	reloadCompanySelectors()
    getCompanies(function (companies) {
        displayCompaniesTree(companies)
    })
}

function reloadCompanySelectors() {
    reloadCompanySelector("addCompanySelect", "Parent Company")
    reloadCompanySelector("deleteCompanySelect", "Company")
    reloadCompanySelector("editCompanySelect", "Company")
}

function reloadCompanySelector(id, firstOption) {

    var selector = $('#' + id)
	selector.empty()

	selector.append($('<option>', {
		value: '',
		text: firstOption
	}));

	getCompanies(function (companies) {
		companies.forEach(function (company) {
			selector.append($('<option>', {
				value: company._id,
				text: company.name
			}));
		})
	})
}


function addPressed() {
    var name = $('#nameInput').val()
    if (name.length == 0) {
        return;
    }
    
	var id = new Date().toUTCString()
	var earnings = parseInt($('#earningsInput').val(), 10)
	var parentCompanyID = $('#addCompanySelect').val()
	var company = new Company(id, name, earnings, parentCompanyID)
	addCompany(company)
}

function deletePressed() {
    var companyID = $('#deleteCompanySelect').val()
    if (companyID.length > 0) {
	   deleteCompany(companyID)
    }
}

function editPressed() {
    var id = $('#editCompanySelect').val()
    var name = $('#editNameInput').val()
    
    if (id.length == 0 || name.length == 0) {
        return
    }
    
    var earnings = parseInt($('#editEarningsInput').val(), 10)
    
    getCompany(id, function(company) {
        company.name = name
        company.earnings = earnings
        editCompany(company)
    })
}





function displayCompaniesTree(companies) {
    var list = $('#treeList')
    list.empty()
    companies.forEach(function (company) {

                if (company.parentID == '' || company.parentID == null) {
                    
                    var sumEarnings = sumEarningsOfCompany(companies, company)
                    
                    var item = $("<li>").html(company.name + ' ~ ' + company.earnings + ' ~ ' + sumEarnings);
                    list.append(item);
                    var subList = $('<ul>');
                    item.append(subList);
                    checkChild(companies, company._id, subList);                    
                }
		})
}

function checkChild(companies, parentid, list) {

    		companies.forEach(function (company) {
    
                if (company.parentID == parentid) {
                    
                                    var sumEarnings = sumEarningsOfCompany(companies, company)
                    
                    
                    var item = $("<li>").html(company.name + ' ~ ' + company.earnings + ' ~ ' + sumEarnings);
                    var subList = $('<ul>');
                    list.append(item);
                    item.append(subList);
                    checkChild(companies, company._id, subList);                    
                } else {
                    return;
                }
		      })
}


